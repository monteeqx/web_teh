var {technologiesController}=require('./technologies-controller');
var {addTechnologyController}=require('./add-technology-controller');
var {indexController}=require('./index-controller');
var {handleTechnologyController}=require('./handle-technology-controller');

module.exports.addTechnologyController=addTechnologyController;
module.exports.technologiesController=technologiesController;
module.exports.indexController=indexController;
module.exports.handleTechnologyController=handleTechnologyController;