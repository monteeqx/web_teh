var {Wtex}=require('../models');

module.exports.handleTechnologyController=(req,res)=>{
    let name=req.body.name;
    let hours=req.body.hours;
    let teacherName=req.body.teacherName;
    Wtex.create({
        name:name,
        hours:hours,
        teacherName:teacherName
    })
    .then(data=>{
        res.redirect('/technologies');
    });
}