function Person(name)
{
this.name = name;
this.sayName = function()
 {
    console.log(name);
 }
}

function Student(name, studentCard, age)
{
    this.studentCard=studentCard;
    this.age=age;
    Person.call(this, name);
    this.sayName=function()
    {
        console.log('Name '+this.name+'; Age '+this.age);
    }
}

var per=new Person('Vasya')
per.sayName();

var stud = new Student('vasya2', 'qweq131', 16);
stud.sayName();