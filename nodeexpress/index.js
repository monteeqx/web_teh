const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser')
const {router} = require('./routes/route');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(router);

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');


var server = app.listen(3000, function(){
	var host = server.address().address
	var port = server.address().port

console.log("App listening at http://%s:%s", host, port)
});
