var {Student} = require('../models/Student');

module.exports.studentList = (req, res) => {

	Student.getInfo()
	.then(data => {
		res.render('home', {students: data});
	});
}