var {Student} = require('../models/Student');

module.exports.addStudent = (req, res) => {
    if(req.body.name){
        Student.addStudent({
            name: req.body.name,
            surname: req.body.surname,
            age: req.body.age,
            studid: req.body.studid,
            group: req.body.group
        })
       /* .then(res => {
            res.redirect('/');
        });*/
    }
    
    res.render('add');
}