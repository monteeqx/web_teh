let routes=[];
let middlewares=[];


let registerRoute=(url,method,callback)=>{
    routes.push({
        url:url,
        method:method,
        callback:callback
    });
}


let useMiddleware=middleware=>{
     middlewares.push(middleware);
}

let callMiddlewares=(req,res)=>{
     for(let i=0;i<middlewares.length;i++){
         middlewares[i](req,res);
     }
}

let bootstrap=(req,res)=>{

    for(let i = 0; i<routes.length;i++){
        if(routes[i].url===req.url && routes[i].method===req.method){
            callMiddlewares(req,res);
            routes[i].callback(req,res);
            return;
        }
    }
    
    notFound404(req,res);
}

let notFound404 = (req, res) => {
    res.write('URL and METHOD wasnt register');
    res.end();
}

let setNotFound= callback=>{
    notFound404=callback;
}

exports.registerRoute=registerRoute;
exports.bootstrap=bootstrap;
exports.setNotFound=setNotFound;
exports.useMiddleware=useMiddleware;