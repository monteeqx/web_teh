var {view} =require('../view-engine/view-engine');

let StudentFormController=(req,res)=>{
    view(req,res,'addForm');
}

exports.StudentFormController=StudentFormController;