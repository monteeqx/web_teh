var {indexController}=require('./index-controller');
var {notFoundController}=require('./notFoundController');
var {StudentListController}=require('./student-list-controller');
var {StudentFormController}=require('./student-form-controller');
var {addStudentController}=require('./add-student-controller');

exports.indexController=indexController;
exports.notFoundController=notFoundController;
exports.StudentListController=StudentListController;
exports.StudentFormController=StudentFormController;
exports.addStudentController=addStudentController;